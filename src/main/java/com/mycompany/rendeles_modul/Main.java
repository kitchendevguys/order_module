/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.rendeles_modul;

import com.mycompany.rendeles_modul.Controllers.RestaurantController;
import io.javalin.Javalin;
import static io.javalin.apibuilder.ApiBuilder.post;
import static io.javalin.apibuilder.ApiBuilder.ws;

/**
 *
 * @author Bálint
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Javalin app = Javalin.create(
            config -> {
                config.enableCorsForAllOrigins();
//                config.addStaticFiles("C:\\C\\Tan\\Egyeb\\WebApp\\Javalin\\javalin-async-example\\src\\main\\resources\\public",
//                        Location.EXTERNAL);
                
                // Set the access-manager that Javalin should use
//                config.accessManager((handler, ctx, permittedRoles) -> {
//                    // ha az endpointot ANYONE használhatja 
//                    // akkor ne is nézzük meg az Authentication headert
//                    if(permittedRoles.contains(UserRoles.ANYONE)
//                        || permittedRoles.isEmpty())
//                    {
//                        System.out.println("Request tovabbitva feldolgozasra");
//                        handler.handle(ctx);
//                    }else{
//                       UserRoles userRole = UserController.getRole(ctx);
//                       if (userRole == UserRoles.ADMIN || permittedRoles.contains(userRole)) {
//                           System.out.println("Request tovabbitva feldolgozasra");
//                           handler.handle(ctx);
//                       } else {
//                           System.out.println("Request megakasztva AccessManager altal");
//                           ctx.status(401).result("Unauthorized");
//                       }   
//                    }
//                });
            }
        ).start(7000);
        
        app.routes(()->{
            //WebSocket
            ws("/websocket-connect/:id", RestaurantController::connectSocket);
            post("/restaurants/:id/orders", RestaurantController::newOrder);
        });
        System.out.println("Server started succesfully");
    }
    
}
