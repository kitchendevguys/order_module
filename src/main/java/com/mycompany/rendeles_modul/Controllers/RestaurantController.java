/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.rendeles_modul.Controllers;

import DataClasses.Order;
import DataClasses.SocketMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.javalin.http.Context;
import io.javalin.websocket.WsContext;
import io.javalin.websocket.WsHandler;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Bálint
 */
public class RestaurantController {
    private static Map<WsContext, Integer> userMap = new ConcurrentHashMap<>();
    
        private static void broadcastMessage(SocketMessage messageObject) {
            userMap.keySet().stream().filter(ctx -> ctx.session.isOpen()).forEach(session -> {
            session.send(messageObject);
        });
    }
    
    public static void newOrder(Context ctx){
        System.out.println("new Order for restaurant Id: "+ctx.pathParam("id"));
        Order newOrder = ctx.bodyAsClass(Order.class);
        SocketMessage sm = new SocketMessage("new_order", newOrder); //TODO: new_order -> ENUM
        broadcastMessage(sm);
        ctx.json("ok");
    }
    
        public static void connectSocket(WsHandler ws){
        ws.onConnect(ctx -> {
            ctx.session.setIdleTimeout(Long.MAX_VALUE);
            System.out.println("ws.onConnect: ");
            System.out.println("id: "+ctx.pathParam("id"));
            userMap.put(ctx, Integer.parseInt(ctx.pathParam("id")));
        });
        ws.onClose(ctx -> {
            userMap.remove(ctx);
            System.out.println("ws.onClose");
        });
        ws.onMessage(ctx -> {
            //broadcastMessage(userUsernameMap.get(ctx), ctx.message());
        });
    }
    
}
