/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataClasses;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Bálint
 */
public class Order {
    List<OrderLine> orderLine = new ArrayList<>();

    public List<OrderLine> getOrderLine() {
        return orderLine;
    }

    public Order(@JsonProperty("orderLine") List<OrderLine> orderLine) {
        this.orderLine = orderLine;
        System.out.println("Order object:");
        for(OrderLine o : orderLine){
            System.out.println("foodId: "+o.getFoodId()+", qty: "+o.getQty());
        }
    }
    
}
