/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataClasses;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 *
 * @author Bálint
 */
public class OrderLine {
    private int foodId;
    private int qty;
    public OrderLine(
        @JsonProperty("foodId")int foodId,
        @JsonProperty("qty")int qty   
    ){
        this.foodId = foodId;
        this.qty = qty;
    }

    public int getFoodId() {
        return foodId;
    }

    public void setFoodId(int foodId) {
        this.foodId = foodId;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
